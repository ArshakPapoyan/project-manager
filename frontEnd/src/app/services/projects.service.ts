import { Injectable } from '@angular/core';
import {BaseService} from './base.service';
import {Observable} from 'rxjs';
import {catchError, retry, tap} from 'rxjs/operators';
import {Project} from '../model/Project'
@Injectable({
  providedIn: 'root'
})
export class ProjectsService extends BaseService{

  private apiUrl = 'http://localhost:8080/api/v1/projects';

  getProjects(): Observable<Project[]> {
    return this.http.get<Project[]>(
      this.apiUrl,
      this.httpOptions
    ).pipe(retry(3),
      tap(_ => console.log('get all projects'),
        catchError(this.handleError<Project[]>('getProjects', []))))
  }

  getProjectById(id: number): Observable<Project> {
    return this.http.get<Project>(
     `${this.apiUrl}/${id}`,
      this.httpOptions
    ).pipe(retry(3),
      tap(_ => console.log(`get project by id = ${id}`),
        catchError(this.handleError<Project>('getProjectById'))))
  }


  addProject(project: Project): Observable<Project> {
    return this.http.post<Project>(
      this.apiUrl, project,
      this.httpOptions
    ).pipe(retry(3),
      tap(_ => console.log('successfully added'))
    );
  }

  updateById(id: number,body: any): Observable<any> {
    return this.http.put<any>(
      `${this.apiUrl}/${id}`,
      body
      ).pipe(
      tap(_ => console.log('successfully updated'))
    );
  }

  deleteById(id: number): Observable<any> {
    return this.http.delete<any>(
      `${this.apiUrl}/${id}`
    ).pipe(
      tap(_ => console.log('successfully deleted')),
      catchError(this.handleError<any>('deleteById'))
    );
  }
}
