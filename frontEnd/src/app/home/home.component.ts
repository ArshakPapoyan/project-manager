import { Component, OnInit } from '@angular/core';
import {ProjectsService} from '../services/projects.service';
import {Project} from '../model/Project';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{
  constructor(private router: Router,
    private projectsService: ProjectsService) {
  }

  projects : Project[] = [];
  isHidden: boolean = true;

  ngOnInit(): void {
    this.getProjects();
  }

  getProjects() {
    this.projectsService.getProjects()
      .subscribe(response => {
        this.projects = response
      });
  }

  deleteProject(id : number){
    this.projects = this.projects.filter(e => e.id !== id);
    this.projectsService.deleteById(id).subscribe(response => console.log(response));
  }

  selectProject(project:Project){
    this.router.navigate(['/projects',project.id])
  }
}
