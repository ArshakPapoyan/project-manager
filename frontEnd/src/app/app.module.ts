import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { Routes, RouterModule} from '@angular/router';
import { AppComponent } from './app.component';
import { UpdateProjectFormComponent } from './project.forms/update/update.project.form..component';
import { CreateProjectFormComponent } from './project.forms/create/create.project.form.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import {ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
  {path: '', redirectTo: '/projects', pathMatch: 'full'},
  {path: 'projects',component: HomeComponent},
  {path: 'projects/:id',component: UpdateProjectFormComponent},
  {path: 'projects-add',component: CreateProjectFormComponent},
  {path: '**',component: NotFoundComponent},

]

@NgModule({
  declarations: [
    AppComponent,
    UpdateProjectFormComponent,
    CreateProjectFormComponent,
    NotFoundComponent,
    HomeComponent,
  ],
    imports: [
        BrowserModule,
        HttpClientModule,
        RouterModule.forRoot(routes),
        ReactiveFormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
