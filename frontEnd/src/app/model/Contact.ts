export class Contact {
  id: number = 0;
  contact: string = '';
  email: string = '';
  phone: string = '';
}
