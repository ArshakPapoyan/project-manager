import {Contact} from './Contact';

export class Project {
  id: number = 0;
  title: string = '';
  status: string = '';
  contacts: Contact[] = [];
}
