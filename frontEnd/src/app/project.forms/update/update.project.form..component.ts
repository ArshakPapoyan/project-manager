import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProjectsService} from '../../services/projects.service';
import {BaseProjectFormComponent} from '../base.project․form.component';

@Component({
  selector: 'app-project',
  templateUrl: './update.project.form.component.html',
  styleUrls: ['../base.project.from.component.css']
})
export class UpdateProjectFormComponent extends BaseProjectFormComponent implements OnInit {

  private readonly id: number;
  error: boolean = false;

  constructor(protected projectService: ProjectsService,
              private activatedRoute: ActivatedRoute,
              protected router: Router) {
    super(projectService, router);
    this.id = parseInt(<string> this.activatedRoute.snapshot.paramMap.get('id'));
  }

  ngOnInit() {
    this.getProjectById(this.id);
  }

  getProjectById(id: number) {
    this.projectService.getProjectById(id).subscribe(res => {
      this.project = res;
      this.formGroupInit();
    });
  }

  onSubmit() {
    super.onSubmit();
    this.projectService.updateById(this.id, this.project).subscribe(_ => {
      this.router.navigate(['/projects']);
    }, (err) => this.error = true);
  }
}
