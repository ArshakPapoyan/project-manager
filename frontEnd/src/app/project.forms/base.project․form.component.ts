import {FormControl, FormGroup, Validators} from '@angular/forms';
import {StylesPattern} from '../util/StylesPattern';
import {ProjectsService} from '../services/projects.service';
import {Project} from '../model/Project';
import {Contact} from '../model/Contact';
import {Regexp} from '../util/Regexp';
import {Router} from '@angular/router';

export class BaseProjectFormComponent {

  project: Project = new Project();
  contacts: Contact[] = [];
  contactFormNamePrefix = 'contactForm_';
  error: boolean = false;

  constructor(protected projectService: ProjectsService,protected router: Router) {
    this.project = new Project();
  }

  projectForm: FormGroup = new FormGroup({
    title: new FormControl(null),
    status: new FormControl(null)
  });

  formGroupInit() {
    this.projectForm.setControl('title', new FormControl(this.project.title, [Validators.required]));
    this.projectForm.setControl('status', new FormControl(this.project.status, [Validators.required]));
    this.project.contacts.forEach((c, i) => {
      this.projectForm.addControl(this.contactFormNamePrefix + i, new FormGroup(
        {
          contact: new FormControl(c.contact, [Validators.required]),
          email: new FormControl(c.email, [Validators.required, Validators.pattern(Regexp.emailRegexp)]),
          phone: new FormControl(c.phone, [Validators.pattern(Regexp.phoneRegexp)]),
        }
      ));
    });
  }

  addContactForm() {
    let newContact = new Contact();
    this.projectForm.addControl(this.contactFormNamePrefix + this.project.contacts.length, new FormGroup(
      {
        contact: new FormControl(newContact.contact, [Validators.required]),
        email: new FormControl(newContact.email, [Validators.required, Validators.pattern(Regexp.emailRegexp)]),
        phone: new FormControl(newContact.phone, [Validators.pattern(Regexp.phoneRegexp)]),
      }
    ));
    this.project.contacts.push(newContact);
  }

  checkError(control: any, event: any) {
    if ((control instanceof FormControl && control.errors) // check control type and containing errors
      || (control instanceof FormGroup && control.get(event.target.name)?.errors)) {

      event.target.style.boxShadow = StylesPattern.errorBoxShadow; // set error styles

      if ((control instanceof FormGroup && control.get(event.target.name)?.errors) // check error type
        && control.get(event.target.name)?.errors?.pattern) {
        event.target.value = 'Invalid format';
      } else {                                        // set error message oriented by error type
        event.target.value = 'Field is mandatory';
      }
    } else {
      event.target.style.boxShadow = '';
    }
  }

  cleanErrors(control: any, event: any) {
    if ((control instanceof FormControl && control.errors)
      || (control instanceof FormGroup && control.get(event.target.name)?.errors)) {
      event.target.style.boxShadow = '';
      event.target.value = '';
    }
  }

  removeContactForm(index: number) {
    this.projectForm.removeControl(this.contactFormNamePrefix + index);
    this.project.contacts.splice(index, 1);
  }

  onSubmit() {
    let values = this.projectForm.value;
    for (const valuesKey in values) {
      if (values.hasOwnProperty(valuesKey)) {
        if (valuesKey === 'title' || valuesKey === 'status') {
          this.project[valuesKey] = values[valuesKey];
        } else if (valuesKey.startsWith(this.contactFormNamePrefix)) {
          let contactIndex = +valuesKey.split('_')[1];
          this.project.contacts[contactIndex] = values[valuesKey];
        }
      }

    }
  }
}
