import {Component, OnInit} from '@angular/core';
import {ProjectsService} from '../../services/projects.service';
import {BaseProjectFormComponent} from '../base.project․form.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-project-add',
  templateUrl: './create.project.form.component.html',
  styleUrls: ['../base.project.from.component.css']
})
export class CreateProjectFormComponent extends BaseProjectFormComponent implements OnInit {


  constructor(protected projectService: ProjectsService, protected router: Router) {
    super(projectService, router);
  }

  ngOnInit(): void {
    this.formGroupInit();
  }

  onSubmit() {
    super.onSubmit();
    this.projectService.addProject(this.project).subscribe(_ => {
        this.router.navigate(['/projects']);
      },
      (err) => this.error = true);
  }

}
