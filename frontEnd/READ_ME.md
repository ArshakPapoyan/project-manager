## Endpoints

## Requirements

    NodeJS
    npm

## Installation

run ```npm init```

## Run

```npm start```

## Projects list page

Fetches data about all projects.

### URL

    /projects

*****************************************************

## Projects edit page

Fetches data about a single project.

### URL

    /projects/:id

### URL Params

Required:

    id=[integer]


*****************************************************

## Project creating paje

Create and save new project

### URL

    /projects-add

