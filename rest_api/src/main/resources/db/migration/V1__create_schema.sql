USE projects_db;

CREATE TABLE IF NOT EXISTS projects
(
    id     INT PRIMARY KEY AUTO_INCREMENT,
    title  VARCHAR(50) NOT NULL,
    status TINYINT     NOT NULL

);
CREATE TABLE IF NOT EXISTS contacts
(
    id         INT PRIMARY KEY AUTO_INCREMENT,
    contact    VARCHAR(50) NOT NULL,
    email      VARCHAR(50) NOT NULL,
    phone      VARCHAR(50),
    project_id INT(11),
    FOREIGN KEY (project_id) REFERENCES projects (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT CC UNIQUE (email, project_id)
);