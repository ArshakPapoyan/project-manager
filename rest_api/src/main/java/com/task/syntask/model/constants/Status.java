package com.task.syntask.model.constants;

public enum Status {
    REJECTED, PROCESS, DONE
}
