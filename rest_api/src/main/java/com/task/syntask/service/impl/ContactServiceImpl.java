package com.task.syntask.service.impl;

import com.task.syntask.repository.ContactRepository;
import com.task.syntask.service.ContactService;
import org.springframework.stereotype.Service;

@Service
public class ContactServiceImpl implements ContactService {

    private final ContactRepository contactRepository;

    public ContactServiceImpl(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @Override
    public void clean() {
        contactRepository.clean();
    }

}
