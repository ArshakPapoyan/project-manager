package com.task.syntask.service.impl;

import com.task.syntask.exceptions.InvalidFormatException;
import com.task.syntask.exceptions.NotFoundException;
import com.task.syntask.model.Contact;
import com.task.syntask.model.Project;
import com.task.syntask.model.constants.Status;
import com.task.syntask.repository.ProjectRepository;
import com.task.syntask.service.ContactService;
import com.task.syntask.service.ProjectService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Service
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;
    private final ContactService contactService;

    public ProjectServiceImpl(ProjectRepository projectRepository, ContactService contactService) {
        this.projectRepository = projectRepository;
        this.contactService = contactService;
    }

    @Override
    public void addProject(Project project) {
        try {
            projectRepository.save(project);
        } catch (Exception e) {
            throw new InvalidFormatException(e.getMessage());
        }
    }

    @Override
    public void removeById(int id) {
        if (!checkById(id)) {
            throw new NotFoundException();
        }
        projectRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void updateById(int id, Project updated) {
        if (!checkById(id)) {
            throw new NotFoundException();
        }
        Status status = updated.getStatus();
        String title = updated.getTitle();
        List<Contact> contacts = updated.getContacts();
        try {
            if (Objects.nonNull(status))
                projectRepository.updateProjectStatusById(id, status.ordinal());
            if (Objects.nonNull(title))
                projectRepository.updateProjectTitleById(id, title);
            if (Objects.nonNull(contacts)) {
                Optional<Project> optionalProject = projectRepository.findById(id);
                if (optionalProject.isPresent()) {
                    Project project = optionalProject.get();
                    project.setContacts(contacts);
                    projectRepository.save(project);
                    contactService.clean();
                }
            }
        } catch (Exception e) {
            throw new InvalidFormatException(e.getMessage());
        }

    }

    @Override
    public Optional<Project> getById(int id) {
        return projectRepository.findById(id);
    }

    @Override
    public List<Project> getAll() {
        return projectRepository.findAll();
    }

    @Override
    public boolean checkById(int id) {
        return this.projectRepository.existsById(id);
    }

}
