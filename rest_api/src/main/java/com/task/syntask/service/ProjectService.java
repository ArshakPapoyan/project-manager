package com.task.syntask.service;

import com.task.syntask.model.Project;

import java.util.List;
import java.util.Optional;

public interface ProjectService {

    void addProject(Project project);

    void removeById(int id);

    void updateById(int id, Project updated);

    Optional<Project> getById(int id);

    List<Project> getAll();

    boolean checkById(int id);

}
