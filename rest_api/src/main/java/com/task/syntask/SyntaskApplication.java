package com.task.syntask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SyntaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(SyntaskApplication.class, args);
    }

}
