package com.task.syntask.controller;

import com.task.syntask.exceptions.InvalidFormatException;
import com.task.syntask.exceptions.NotFoundException;
import com.task.syntask.model.Project;
import com.task.syntask.service.ProjectService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1/projects")
public class ProjectController {

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping
    public ResponseEntity<List<Project>> getAll() {
        return ResponseEntity.ok(projectService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Project> getByTitle(@PathVariable("id") Integer id) {
        Optional<Project> byId = projectService.getById(id);
        return byId.map(ResponseEntity::ok).orElseThrow(NotFoundException::new);

    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> addProject(@Valid @RequestBody Project project) {
        projectService.addProject(project);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProject(@PathVariable Integer id) {
        projectService.removeById(id);
        return ResponseEntity.ok("Deleted project with id = " + id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateProject(@Valid @PathVariable Integer id,
                                                @RequestBody Project updatedData) {
        projectService.updateById(id, updatedData);
        return ResponseEntity.ok("Updated project with id = " + id);
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(InvalidFormatException.class)
    public Map<String, String> handleValidationExceptions(
            InvalidFormatException ex) {
        Map<String, String> errors = new HashMap<>();
        errors.put("Error",ex.getMessage());
        return errors;
    }
}
