package com.task.syntask.repository;

import com.task.syntask.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {

    @Modifying
    @Query(value = "DELETE FROM contacts WHERE project_id IS NULL")
    void clean();

}
