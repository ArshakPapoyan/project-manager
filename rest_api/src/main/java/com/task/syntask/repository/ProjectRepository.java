package com.task.syntask.repository;

import com.task.syntask.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {

    @Modifying
    @Query(value = "UPDATE projects SET title = ?2 WHERE id = ?1", nativeQuery = true)
    void updateProjectTitleById(Integer id, String title);

    @Modifying
    @Query(value = "UPDATE projects SET status = ?2 WHERE id = ?1", nativeQuery = true)
    void updateProjectStatusById(Integer id, int status);

    boolean existsById(int id);
}
