## REST API

## Requirements

    JDK 14

    Maven 3

    MySQL

## Installation

Make mysql database and set url in 'application.properties'

run ```mvn clean install```

## Run

```mvn spring-boot:run```

## Get all projects

Returns json data about all projects.

### URL

    /api/v1/projects

### Method:

    GET

### URL Params

Required:

    None

Data Params

    None

Success Response:

    Code: 200

Content:

```JavaScript
[{
    id: 12,
    title: "MyProject",
    status: "DONE",
    contacts: [{id: 2, contact: "John", email: "john@gmail.com", phone: +37495661457}]
}]
```

### Error Response:

    None

Sample Call:

```JavaScript
$.ajax({
    url: "/api/v1/projects", dataType: "json", type: "GET", success: function (r) {
        console.log(r);
    }
});
```

*****************************************************

## Get single project

Returns json data about a single project.

### URL

    /api/v1/projects/:id

### Method:

    GET

### URL Params

Required:

    id=[integer]

Data Params

    None

Success Response:

    Code: 200

Content:

```JavaScript 
{
    id : 12, 
    title : "MyProject",
    status:"DONE",
    contacts : [{id:2, contact: "John", email:"john@gmail.com",phone: +37495661457}]               
}
```

### Error Response:

    Code: 404

Sample Call:

```JavaScript
$.ajax({
    url: "/api/v1/projects/12",
    dataType: "json",
    type: "GET",
    success: function (r) {
        console.log(r);
    }
});
```

*****************************************************

## Create new project

Create and save new project entity

### URL

    /api/v1/projects

### Method:

    POST

### URL Params

Required:

    None

Request body:

```JavaScript
{
    title : "MyProject",
    status : "DONE", 
    contacts:[{contact: "John", email: "john@gmail.com", phone: "+37495661457"}]
}
```

### Success Response:

    Code: 200

### Error Response:

    Code: 422 Message : Please check your input format

### Sample Call:
```JavaScript
$.ajax({ url: "/api/v1/projects", dataType: "json", type : "POST", });
```
*****************************************************

## Update project

Update existing project entity

### URL

    /api/v1/projects/:id

### Method:

    PUT

### URL Params

Required:

    id=[integer]

Request body:
```JavaScript
{
    title : "MyProject",
    status: "DONE",
    contacts : [{contact: "John", email:"john@gmail.com",phone: "+37495661457"}]               
}
```
### Success Response:

    Code: 200 Message : Updated project with id = 12

### Error Response:

    Code: 422 Message : Please check your input format

    Code: 404 Message: project not found

### Sample Call:
```JavaScript
$.ajax({ url: "/api/v1/projects/12", dataType: "json", type : "PUT", });
```
*****************************************************

## Delete project

Delete existing project entity

### URL

    /api/v1/projects/:id

### Method:

    DELETE

### URL Params

Required:

    id=[integer]

Request body:

    None

### Success Response:

    Code: 200 Message : Deleted project with id = 12

### Error Response:

    Code: 404 Message: project not found

### Sample Call:
```JavaScript
$.ajax({ url: "/api/v1/projects/12", type : "DELETE"});
```